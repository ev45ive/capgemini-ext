/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.TestController', {
    extend: 'Ext.app.ViewController',
    alias:'controller.testcontroller',

    /**
     * Called when the view is created
     */
    init: function() {
        console.log('init')
    },

    initViewModel: function () {

    },

    destroy: function(){
        console.log('destroy')
    }
});