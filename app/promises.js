function makePromise(obj,func, params){
    return new Ext.Promise(function(resolve,reject){
        obj[func].apply(obj, params.concat({ success: resolve, failure: reject }))
    })
}


var futureTask = makePromise(TrainingApp.model.Task,'load',[41])


p = futureTask

    .then(function(task){
        return makePromise(task,'getUser',[])
    })

    .then(function(task){
        return task.get('name')
    })

p.then(function(title){
    console.log(title)
}, function (err) {
    console.error('err',err)
})


p = new Ext.Promise(function(resolve,reject){ resolve('awesome') })

p.then(function(data){ console.log(data) })