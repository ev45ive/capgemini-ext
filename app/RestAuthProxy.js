/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.RestAuthProxy', {
    extend:'Ext.data.proxy.Rest',
    alias:'proxy.rest-auth-proxy',

    requires: [
        'TrainingApp.Config'
    ],

    require:[
        'TrainingApp.Config'
    ],

    doRequest: function () {
        this.headers = this.headers || {}
        this.headers['Authorization'] = 'Bearer '+TrainingApp.Config.token

        this.callParent(arguments)
    }

});