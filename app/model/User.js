/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.model.User', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest',
        'Ext.data.validator.Length',
        'Ext.data.validator.Presence'
    ],

    fields: [
        { name: 'id', type: 'int'},
        { name: 'name', type:'string'},
        { name: 'email', type:'string'},
        { name: 'address', type:'auto' },
        // generate field value when writing
        { name: 'fullName', type: 'string', convert: function (v,record) {
            return record.data.name + ' ' + record.data.username
        }},
        // generate field value when reading
        { name: 'fullAddress', type:'string', calculate:function (user) {
            return user.address && ( user.address.city + ', ' + user.address.street)
        }}
        /*
        The fields for this model. This is an Array of Ext.data.field.Field definition objects or simply the field name.
        If just a name is given, the field type defaults to auto.  For example:

        { name: 'name',     type: 'string' },
        { name: 'age',      type: 'int' },
        { name: 'phone',    type: 'string' },
        { name: 'gender',   type: 'string' },
        { name: 'username', type: 'string' },
        { name: 'alive',    type: 'boolean', defaultValue: true }
         */
    ],

    validators: {
        id: 'presence',
        name:{ type:'length', min:2 }
        /*
        Uncomment to add validation rules

            age: 'presence',
            name: { type: 'length', min: 2 },
            gender: { type: 'inclusion', list: ['Male', 'Female'] },
            username: [
                { type: 'exclusion', list: ['Admin', 'Operator'] },
                { type: 'format', matcher: /([a-z]+)[0-9]{2,3}/i }
            ]
        }
        */
    },

    /*
    Uncomment to add a rest proxy that syncs data with the back end.
    */
    proxy: {
        type: 'rest',
        url : 'http://localhost:3000/users',
        writer:{
            writeAllFields: true
        }
    }
});