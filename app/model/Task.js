/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.model.Task', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        { name:'userId', type:'int', reference: 'TrainingApp.model.User'},
        { name:'id', type:'int'},
        { name:'title', type:'string'},
        { name:'dueDate', type:'date'},
        { name:'completed', type:'boolean', defaultValue: false},
        /*
        The fields for this model. This is an Array of Ext.data.field.Field definition objects or simply the field name.
        If just a name is given, the field type defaults to auto.  For example:

        { name: 'name',     type: 'string' },
        { name: 'age',      type: 'int' },
        { name: 'phone',    type: 'string' },
        { name: 'gender',   type: 'string' },
        { name: 'username', type: 'string' },
        { name: 'alive',    type: 'boolean', defaultValue: true }
         */
    ],

    /*
    Uncomment to add validation rules
    validators: {
        age: 'presence',
        name: { type: 'length', min: 2 },
        gender: { type: 'inclusion', list: ['Male', 'Female'] },
        username: [
            { type: 'exclusion', list: ['Admin', 'Operator'] },
            { type: 'format', matcher: /([a-z]+)[0-9]{2,3}/i }
        ]
    }
    */

    getUsersPromise: function () {
        var task = this;
        return new Ext.Promise(function (resolve,reject) {
              task.getUsers({
                  success:resolve, failure: reject
              })
        })
    },

    /*
    Uncomment to add a rest proxy that syncs data with the back end.
    */
    proxy: {
        type: 'rest',
        url : 'http://localhost:3000/todos'
    }
});