/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.view.TestView', {
    extend: 'Ext.Container',
    xtype: 'testview',

    requires: [
        'TrainingApp.TestController'
    ],

    controller:'testcontroller',

    /*
    Uncomment to give this component an xtype
    xtype: 'test',
    */

    items: [
        {
            xtype:'textfield',
            reference:'text'
        },
        /* include child components here */
        {
            xtype: 'button',
            text:'update',
            handler: 'onUpdate'
        }
    ]
});