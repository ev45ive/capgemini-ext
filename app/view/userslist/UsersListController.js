/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.userslist.UsersListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.userslist',

    /**
     * Called when the view is created
     */
    init: function() {

    },

    requires:[
        'TrainingApp.model.User'
    ],

    onSelectionChange: function (view, records) {
        this.lookupReference('removeBtn').setDisabled(!records.length);
    },

    createNew: function () {
        var grid = this.getView(),
            plugin = grid.findPlugin('rowediting'),
            user = Ext.create('TrainingApp.model.User');
            user.set('id',null)
            store = grid.getStore();


        plugin.cancelEdit();

        store.insert(0, user);
        plugin.startEdit(user, 0);
    },

    removeUser: function () {
        var grid = this.getView(),
            plugin = grid.findPlugin('rowediting'),
            selModel = grid.getSelectionModel(),
            store = grid.getStore();

        plugin.cancelEdit();
        store.remove(selModel.getSelection());
        if (store.getCount() > 0) {
            selModel.select(0);
        }
    },

    onEdit: function (widget, event) {
        var grid = this.getView()
        var store = grid.getStore()
        var record = widget.getWidgetRecord()

        this.redirectTo('user/'+record.get('id'))
        //Ext.Msg.alert( record.data.name)
    }
});