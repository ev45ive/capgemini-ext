/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.userslist.UsersList', {
    extend: 'Ext.grid.Panel',
    xtype: 'view.userlist',

    requires: [
        'Ext.data.Store',
        'Ext.util.Format',
        'TrainingApp.store.UsersList',
        'TrainingApp.view.userslist.UsersListController'
    ],

    controller: 'userslist',

    bind: {
        title: '{currentUser}'
    },

    store: {
        type: 'userslist'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToMoveEditor: 2,
        clicksToEdit:2
    }],

    tbar:[
        '->',
        {
            xtype:'button',
            text:'Add',
            iconCls:'x-fa fa-plus',
            handler:'createNew'
        },
        {
            xtype:'button',
            text:'Remove',
            disabled:true,
            reference: 'removeBtn',
            iconCls:'x-fa fa-minus',
            handler:'removeUser'
        }
    ],

    height: '100%',

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: [{
        text: 'Id',
        flex: 1,
        dataIndex: 'id'
    },{
        text: 'Name',
        flex: 1,
        dataIndex: 'name',
        editor: {
            allowBlank: false,
            minLength:3
        }
    },{
        text: 'Email',
        flex: 1,
        dataIndex: 'email',
        editor: {
            allowBlank: false
        }
    },{
        text: 'Adress',
        flex: 1,
        dataIndex: 'fullAddress',
        // renderer: function (value,col,record) {
        //     return record.get('address.city')
        // }
        // editor: {
        //     allowBlank: false
        // }
    },
    {
        text:'Actions',
        flex:1,
        xtype:'widgetcolumn',
            widget: {
                xtype: 'button',
                text: 'Edit',
                handler: 'onEdit'
            }
    }]

})