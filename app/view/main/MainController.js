/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    requires: [
        'TrainingApp.model.User',
        'TrainingApp.store.UsersList',
        'TrainingApp.view.TestView',
        'TrainingApp.view.taskslist.TasksList',
        'TrainingApp.view.userform.UserForm',
        'TrainingApp.view.userslist.UsersList'
    ],

    store: {
        type: 'userslist'
    },

    routes:{
        'list': 'showList',
        'user/:id': 'showForm',
        'newUser': 'showForm',
        'testView':'showTest',
        'tasks': 'showTasks',
        'tasks/:id': {
            conditions:{
              ':id': '(\\d+)'
            },
            action: 'showTasks'
        },
    },

    showTasks:function (user_id) {
        var view = Ext.widget('taskslist');

        view.getController().setUser(user_id)

        this.changeView(view)
    },

    showTest:function () {
        this.changeView( Ext.widget('testview'))
    },

    showList: function (user_id) {
        this.changeView( Ext.widget('view.userlist'))
    },

    changeView: function (component) {
        var view = this.getView()
        view.removeAll()
        view.add( component )
    },

    showForm: function (user_id) {
        // clear old view:
        var view = this.getView()
        view.removeAll()
        // create new view:
        var form = Ext.widget('userform')

        // show new view
        view.add( form )

        if(!user_id){
            var user = Ext.create('TrainingApp.model.User')
            user.set('id',null);
            form.getViewModel().set('user',user)
            return
        }

        // load data  from server
        TrainingApp.model.User.load(user_id, {
            success: function (user) {
                // update viewModel with user data
                form.getViewModel().set('user',user)
            }
        })
    },


    // changeView: function(view){
    //     var view = this.getView()
    //     view.removeAll()
    //     view.add( Ext.widget('userform'))
    // },

    /**
     * Called when the view is created
     */
    init: function() {

    }
});