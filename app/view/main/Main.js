/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.main.Main', {
    extend: 'Ext.panel.Panel',

    requires: [
        'TrainingApp.view.TestView',
        'TrainingApp.view.main.MainController',
        'TrainingApp.view.userslist.UsersList'
    ],

    title:'Training App',

    controller: 'main',

    viewModel:{
        data:{
            currentUser:'Test User'
        }
    },

   // ui: 'main',

    layout: 'fit',

    items: [

        // {
        //     xtype:'view.userlist',
        //     title: 'Users List'
        // }
    ]
});