/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.view.taskslist.TasksList', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.grid.column.Check',
        'TrainingApp.store.TasksStore',
        'TrainingApp.view.taskslist.TasksListController',
        'TrainingApp.view.taskslist.TasksListModel',
        'TrainingApp.view.taskslist.TitleColumn'
    ],

    xtype: 'taskslist',

    store: {
        type:'tasks'
    },

    viewModel: {
        type: 'taskslist'
    },

    plugins: [{
        ptype: 'rowediting',
        clicksToMoveEditor: 2,
        clicksToEdit:2
    }],


    controller: 'taskslist',

    columns: [
        {
            text:'Id',
            flex:1,
            dataIndex:'id'
        },
        {
            xtype:'titlecolumn',
            text:'Title',
            flex:3,
            dataIndex:'title',
            editor:{
                vtype:'alpha'
            }
        },
        {
            text:'Due Date',
            dataIndex:'dueDate',
            editor:{
                vtype:'time',

            }
        },
        {
            xtype:'checkcolumn',
            text:'Completed',
            flex:1,
            dataIndex:'completed'
        }

    ]
});