/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.view.taskslist.TasksListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.taskslist',

    requires: [
        'Ext.data.StoreManager'
    ],

    /**
     * Called when the view is created
     */
    init: function() {


    },
    setUser: function (user_id) {
        //Ext.StoreManager.lookup('tasksstore')
        var store = this.getView().getStore()

        store.load({
            params: {
                userId: user_id
            }
        })
    }


});