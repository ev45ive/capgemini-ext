/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.view.taskslist.TasksListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.taskslist',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'TasksList',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});