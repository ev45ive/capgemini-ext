/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.view.taskslist.TitleColumn', {
    extend: 'Ext.grid.column.Column',

    xtype: 'titlecolumn',

    titleTmp: new Ext.XTemplate(
        '<div class="{completedTitleCls}">',
        '<tpl if="completed">✔</tpl>',
        '{title}',
        '</div>'
    ),

    defaultRenderer: function(value,meta, record){
        return this.titleTmp.apply({
            title: value,
            completed: record.get('completed')
        })
    }
});