/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.userform.UserForm', {
    extend: 'Ext.Container',

    requires: [
        'TrainingApp.view.userform.UserFormModel',
		'TrainingApp.view.userform.UserFormController'
    ],

    layout:'center',
    margin: '20',

    /*
    Uncomment to give this component an xtype
    */
    xtype: 'userform',


    viewModel: {
        type: 'userform'
    },

    controller: 'userform',

    items: [
        {
            xtype:'form',
            // title: 'Static title'
            reference:'userForm',
            bind: {
                title: ' Edit : {title}'
            },
            items:[
                {
                    xtype: 'textfield',
                    fieldLabel: 'Name:',
                    reference: 'nameField',
                    //value:'Some static value'
                    bind: {
                        value: '{user.name}'
                    }
                },{
                    xtype:'textfield',
                    fieldLabel:'Email:',
                    //value:'Some static value'
                    bind:{
                        value:'{user.email}'
                    }
                }
            ],

            buttons:[
                {
                    xtype:'button',
                    text:'Save',
                    bind:{
                        disabled:'{!formValid}'
                    },
                    listeners:{
                        click: 'saveUser'
                    }
                },
                {
                    xtype:'button',
                    text:'Cancel',
                    handler: 'backToList'
                }
            ]
        }
        /* include child components here */
    ]
});