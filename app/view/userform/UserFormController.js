/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.userform.UserFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.userform',

    /**
     * Called when the view is created
     */
    init: function() {

    },

    backToList: function(){
        this.redirectTo('list')
    },

    saveUser: function () {
        console.log(this.getViewModel())

        this.getViewModel().get('user').save()
        this.redirectTo('list')
    }
});
