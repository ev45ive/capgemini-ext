/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.view.userform.UserFormModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.userform',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'UserForm',
            autoLoad: true
        }
        */
    },

    formulas:{
        userName: function (get) {

        },
        title: function (get) {
            var name = get('user.name');
            return get('user.id')? (name) : 'New User'
        },
        formValid: function (get) {
            console.log(get('userForm'))
            return false;
        }
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
        title: 'Test',
        user:{},
        options:{},
        selections:{}
    }
});