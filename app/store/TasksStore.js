/**
 * Created by ev45i on 8/2/2017.
 */
Ext.define('TrainingApp.store.TasksStore', {
    extend: 'Ext.data.Store',
    alias:'store.tasks',

    requires: [
        'TrainingApp.model.Task'
    ],

    model: 'TrainingApp.model.Task',


   // autoLoad:true,
   // autoSync:true
});