/**
 * Created by ev45i on 8/1/2017.
 */
Ext.define('TrainingApp.store.UsersList', {
    extend: 'Ext.data.Store',
    alias: 'store.userslist',

    storeId: 'userlist',
    //Ext.data.StoreManager.lookup('userlist')

    requires: [
        'TrainingApp.model.User'
    ],

    model:'TrainingApp.model.User',

    /*
    Uncomment to use a specific model class
    model: 'User',
    */

    /*
    Fields can also be declared without a model class:
    fields: [
        {name: 'firstName', type: 'string'},
        {name: 'lastName',  type: 'string'},
        {name: 'age',       type: 'int'},
        {name: 'eyeColor',  type: 'string'}
    ]
    */

    autoLoad: true,
    autoSync: true,
    /*
    Uncomment to specify data inline
    */
    data : [
        {id: 1, name: 'Ed',    email: 'Spencer'},
        {id: 2, name: 'Tommy', email: 'Maintz'},
        {id: 3, name: 'Aaron', email: 'Conran'}
    ]
});